"""tugas1sebelas URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from django.views.generic.base import RedirectView
import update_status.urls as update_status
import add_friend.urls as add_friend
import profil.urls as profile
import statistik.urls as statistik

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^update-status/', include(update_status, namespace='update-status')),
    url(r'^add-friend/', include(add_friend, namespace='add-friend')),
    url(r'^profile/', include(profile, namespace='profile')),
    url(r'^statistik/', include(statistik, namespace='statistik')),
    url(r'^$', RedirectView.as_view(url='update-status/', permanent=True), name='$  '),
]
