from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .views import index, new_friend, url_is_valid
from .models import Friend

# Create your tests here.
class AddFriendUnitTest(TestCase):
	def test_add_friend_url_is_exist(self):
		response = Client().get('/add-friend/')
		self.assertEqual(response.status_code, 200)

	def test_add_friend_using_index_func(self):
		found = resolve('/add-friend/')
		self.assertEqual(found.func, index)

	def test_model_can_add_new_friend(self):
		new_friend = Friend.objects.create(name = 'Jonas Hector', url = 'http://test.com')
		counting_all_available_friends = Friend.objects.all().count()
		self.assertEqual(counting_all_available_friends, 1)

	def test_url_validator(self):
		not_valid_url = 'http://kjgdrtiupmmbzqzxd.com'
		valid_url = 'http://google.com'
		self.assertEqual(url_is_valid(not_valid_url), False)
		self.assertEqual(url_is_valid(valid_url), True)
