from django.conf.urls import url
from .views import index, new_friend

urlpatterns = [
	url(r'^$', index, name='index'),
	url(r'^new_friend', new_friend, name='new_friend'),
]
