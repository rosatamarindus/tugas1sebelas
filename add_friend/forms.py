from django import forms

class Add_Friend_Form(forms.Form):
    my_messages = {
        'required': 'Tolong isi input ini',
        'invalid': 'Isi input dengan url',
    }
    attrs = {
        'class': 'form-control'
    }

    name = forms.CharField(label='Name',required=True, max_length=27, error_messages = my_messages, widget=forms.TextInput(attrs=attrs))
    url = forms.URLField(label='URL',required=True, max_length=200,error_messages = my_messages, widget=forms.TextInput(attrs=attrs))
    #message = forms.CharField(widget=forms.Textarea(attrs=attrs), required=True, error_messages = my_messages)
