from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404
from .forms import Add_Friend_Form
from .models import Friend
from urllib.request import urlopen
from urllib.error import URLError

# Create your views here.
response = {}
def index(request):
	response['author'] = "Hanif" #TODO Implement yourname
	friend = Friend.objects.all()
	response['friend'] = friend
	html = 'add_friend/add_friend.html'
	response['add_friend_form'] = Add_Friend_Form
	return render(request, html, response)

def new_friend(request):
	if(request.method == 'POST' and form.is_valid()):
		response['name'] = request.POST['name']
		response['url'] = request.POST['url']
		friend = Friend(name=response['name'], url=response['url'])
		friend.save()
		return HttpResponseRedirect('/add-friend/')
	else:
		return HttpResponseRedirect('/add-friend/')

def url_is_valid(url):
    try:
        thepage = urlopen(url)
    except URLError as e:
        return False
    else:
        return True
