## Kelompok 11 -- Kelas PPW-D:

1. [X] Rosa Nur Rizky Febrianty Tamarindus - 1606917916 - R
2. [X] M. Hanif Pratama - 1606876065 - H
3. [X] Farolina Rahmatunnisa -1606827870 - F

## Status Pipelines
[![Build status](https://gitlab.com/rosatamarindus/tugas1sebelas/badges/master/build.svg)](https://gitlab.com/rosatamarindus/tugas1sebelas/commits/master)

## Status Code Coverage
[![coverage report](https://gitlab.com/rosatamarindus/tugas1sebelas/badges/master/coverage.svg)](https://gitlab.com/rosatamarindus/tugas1sebelas/commits/master)

## Link Herokuapp
Link herokuapp: https://tugas1sebelas.herokuapp.com/