from django.conf.urls import url
from .views import index, add_post, del_post
#url for app
urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^add_post', add_post, name='add_post'),
	url(r'^del_post/(?P<status_id>\d+)/$', del_post, name='del_post'),
	url(r'^(?P<slug>[\w-]+)/delete/$', del_post),
]
