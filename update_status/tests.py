from django.test import TestCase, Client
from django.urls import resolve
from django.http import HttpRequest
from django.utils import timezone
from .views import index, add_post
from .models import Status
from .forms import Status_Form

class UpdateStatus(TestCase):
	def test_update_status_url_is_exist(self):
		response = Client().get('/update-status/')
		self.assertEqual(response.status_code, 200) #OK

	def test_update_status_using_index_func(self):
		found = resolve('/update-status/')
		self.assertEqual(found.func, index)

	def test_model_can_create_new_status(self):
		# Creating a new status
		new_status =  Status.objects.create(description='So I\'m here to be a winner! Go big or go home ^^')

		# Retrieving all available status
		counting_all_available_status =  Status.objects.all().count()
		self.assertEqual(counting_all_available_status, 1)

	def test_update_status_success_and_render_the_result(self):
		description = 'Coverage, I heart you'
		response_post = Client().post('/update-status/add_post', {'description': description})
		self.assertEqual(response_post.status_code, 302)

		response= Client().get('/update-status/')
		html_response = response.content.decode('utf8')
		self.assertIn(description, html_response)

	def test_update_status_error_and_render_the_result(self):
		description = 'Anonymous'
		response_post = Client().post('/update-status/add_post', {'description':'' })
		self.assertEqual(response_post.status_code, 302)

		response= Client().get('/update-status/')
		html_response = response.content.decode('utf8')
		self.assertNotIn(description, html_response)

	def test_form_todo_input_has_placeholder_and_css_classes(self):
		form = Status_Form()
		self.assertIn('class="status-form-textarea', form.as_p())
		self.assertIn('id="id_description"', form.as_p())

	def test_form_validation_for_blank_items(self):
		form = Status_Form(data={'description': ''})
		self.assertFalse(form.is_valid())
		self.assertEqual(
		    form.errors['description'],
		    ["This field is required."]
		    )