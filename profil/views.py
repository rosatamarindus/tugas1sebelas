from django.shortcuts import render
from .models import Biodata

response = {'author': "Farol"}

def biodata_table(request):
	try:
		biodata = Biodata.objects.get(id=1)
		explist = biodata.expertise.split(',')
		response['biodata'] = biodata
		response['explist'] = explist
	except Biodata.DoesNotExist:
		explist = 'Error'
		response['biodata'] = 'Error'
		response['explist'] = explist
	finally:
		html = 'profil/profil.html'
		return render(request, html , response)