from django.conf.urls import url
from .views import biodata_table

urlpatterns = [
    url(r'^$', biodata_table, name='biodata_table'),
 ]
