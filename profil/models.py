from django.db import models
from django.contrib import admin

class Biodata(models.Model):
		
    name = models.CharField(max_length=27)
    birth = models.DateTimeField()
    sex = models.CharField(max_length=7)
    description = models.TextField()
    email = models.EmailField()
    expertise = models.CharField(max_length=250, default='a,b,c')
    photo = models.TextField(default="https://rosatamarindus.github.io/img/profile.jpg")
