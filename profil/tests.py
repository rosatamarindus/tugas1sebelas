from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .views import biodata_table
from .models import Biodata
from datetime import datetime

# Create your tests here.
class ProfilUnitTest(TestCase):
    def test_profil_url_is_exist(self):
        response = Client().get('/profile/')
        self.assertEqual(response.status_code, 200)
    def test_profil_using_biodata_table_func(self):
        found = resolve('/profile/')
        self.assertEqual(found.func, biodata_table)
