import sys
sys.path.append(sys.path[0] + '/..')

from django.test import TestCase
from django.test import Client
from django.urls import resolve

from update_status.models import Status
from add_friend.models import Friend
from datetime import date
from .views import index


class DashboardUnitTest(TestCase):
    def test_statistik_is_exist(self):
        response = Client().get('/statistik/')
        self.assertEqual(response.status_code, 200)

    def test_statistik_has_status_count(self):
        trial = Status.objects.create(description='Namanya juga trial')

        response = Client().get('/statistik/')
        html_response = response.content.decode('utf8')

        self.assertIn('1 Posts', html_response)

    def test_statistik_has_friend_count(self):
        friend_dummy = Friend.objects.create(name='Rosa Nur', url='http://tugaslingin.com/rosatamarindus')
        response = Client().get('/statistik/')
        html_response = response.content.decode('utf8')
        self.assertIn('1 People', html_response)

    def test_statistik_has_latest_post(self):
        message = "Kamu belum memposting apapun"
        status_trial = Status.objects.create(description='Namanya juga trial')
        response = Client().get('/statistik/')
        html_response = response.content.decode('utf8')
        self.assertNotIn(message, html_response)

    def test_statistik_has_no_latest_post(self):
        no_post = "Oops! Anda belum memposting apapun."
        response = Client().get('/statistik/')
        html_response = response.content.decode('utf8')
        self.assertIn(no_post, html_response)