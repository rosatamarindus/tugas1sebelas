from django.shortcuts import render
from django.http import HttpResponseRedirect
from update_status.models import Status
from profil.views import biodata_table
from add_friend.models import Friend
from datetime import *

response = {}
def index(request):
	response['author'] = "Rosa Nur Rizky F.T"
	#status = Status.objects.filter()[:1].get()
	#response['status']=status
	status_count = len(Status.objects.all())
	response['status_count'] = status_count

	friend_count = len(Friend.objects.all())
	response['friend_count'] = friend_count

	qs = Status.objects.order_by('-created_date')
	if(len(qs) > 0):
		latest_post = qs[0]
	else:
		latest_post = None

	response['latest_post'] = latest_post

	html = 'statistik/statistik.html'

	return render(request, html, response)